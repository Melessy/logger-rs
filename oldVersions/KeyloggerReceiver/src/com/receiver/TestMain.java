package com.receiver;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;

/**
 * Tester main LOL
 */
public class TestMain 
{
    
    /**
    * Main Method
    * @param args
    * @deprecated remove
    */
    public static void main(String[] args) throws FileNotFoundException, IOException
    {
        Files.lines(new File("./data/logs/Users.json").toPath())
       .map(s -> s.trim())
       .filter(s -> !s.isEmpty())
       .forEach(System.out::println);
        }
}
