/*
 * Copyright (c) 2020, Melessy <https://bitbucket.org/Melessy/> 
 * Email: <melessy@tutamail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.receiver;

import static com.receiver.Constants.mainDir;
import com.receiver.fileserver.FileServer;
import com.receiver.fileserver.FileServerHandler;
import com.receiver.textserver.TextServer;
import com.receiver.textserver.TextServerHandler;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main 
{  
    
   public static ExecutorService executor; // Executor variable
    
   /**
    * Main Method
    * @param args
    */
    public static void main(String[] args)
    {
        /**
         * Log to Receiver.xml
         * @TODO Do this a better way..
         */
        try 
        {
            FileHandler fh = new FileHandler("./data/logs/" + Constants.seperator + "Receiver.xml");
            Logger.getGlobal().addHandler(fh);
            Logger.getLogger(TextServer.class.getName()).addHandler(fh);
            Logger.getLogger(FileServer.class.getName()).addHandler(fh);
            Logger.getLogger(TextServerHandler.class.getName()).addHandler(fh);
            Logger.getLogger(FileServerHandler.class.getName()).addHandler(fh);
            Logger.getLogger(Main.class.getName()).addHandler(fh);
            } 
            catch (IOException | SecurityException ex) 
            {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        
            /**
             * Create mainDir
             */
            if (!mainDir.exists())
            {
                mainDir.mkdir();
            }
            
            /**
             * Start servers
             */
            executor = Executors.newCachedThreadPool(); // Initialize executor
            Logger.getGlobal().log(Level.INFO, executor.toString()); // Print executor status
            executor.submit(new TextServer()); // Start TextServer
            executor.submit(new FileServer()); // Start FileServer
    }
}
