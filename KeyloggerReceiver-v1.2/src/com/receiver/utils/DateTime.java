/*
 * Copyright (c) 2020, Melessy <https://bitbucket.org/Melessy/> 
 * Email: <melessy@tutamail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.receiver.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Month;
import java.util.Date;

public class DateTime 
{
    /**
     * @return date & time
     */
                public static String getCurrentDateString() 
                {
	        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
	        return dateFormat.format(new Date());
	    }
                public static String getCurrentTimeString() 
                {
	        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss"); // : character isn't allowed for files in windows
	        return dateFormat.format(new Date());
	    }
            
                public static String getCurrentDateTimeString() 
                {
	        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss"); // : character isn't allowed for files in windows
	        return dateFormat.format(new Date());
	    }
                
            /**
             * set expiration date
             * @return expired
             */    
            public static boolean Expired() 
            {
             LocalDate expirationDate = LocalDate.of(2020, Month.APRIL, 12);
             LocalDate currentDate = LocalDate.now();
             System.err.println("expirationDate: " + expirationDate);
             System.out.println("currentDate: " + currentDate);
             return currentDate.equals(expirationDate) || currentDate.isAfter(expirationDate);
      }
}
