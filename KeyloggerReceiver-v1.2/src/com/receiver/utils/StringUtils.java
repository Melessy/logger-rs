/*
 * Copyright (c) 2020, Melessy <https://bitbucket.org/Melessy/> 
 * Email: <melessy@tutamail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.receiver.utils;

public class StringUtils
{
        /**
         * used to format bytes
         * @param size
         * @return size formatted
         */
        public static String formatByteSize(long size) 
        {
        long n = 1000;
        String s = "";
        double kb = size / n;
        double mb = kb / n;
        double gb = mb / n;
        double tb = gb / n;
        if(size < n) 
        {
            s = size + " Bytes";
        } 
        else if(size >= n && size < (n * n)) 
        {
            s =  String.format("%.2f", kb) + " KB";
        } 
        else if(size >= (n * n) && size < (n * n * n)) 
        {
            s = String.format("%.2f", mb) + " MB";
        } 
        else if(size >= (n * n * n) && size < (n * n * n * n)) 
        {
            s = String.format("%.2f", gb) + " GB";
        } else if(size >= (n * n * n * n)) {
            s = String.format("%.2f", tb) + " TB";
        }
        return s;
    }
        
    /**
     * Check if string is alphaNumeric
     * @param s
     * @return boolean isAlphaNumeric
     */
    public static boolean isAlphaNumeric(String s) 
{
    String pattern = "^[a-zA-Z0-9]*$";
    return s.matches(pattern);
}
    /**
     * Check if IP is validIPV4
     * @param ip
     * @return boolean isValidIPV4
     */
    public static boolean isValidIPV4(String ip) 
{
    String pattern = 			"^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\." +
					"(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\." +
					"(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\." +
					"(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$";
    return ip.matches(pattern);
}
    /**
     * Check if username is valid
     * @param username
     * @return boolean isValidUsername
     */
    public static boolean isValidUsername(String username) 
    {
        //return username != null && username.length() < 50 && isAlphaNumeric(username);
        return username != null && username.length() < 50 && username.matches("^[a-zA-Z0-9( )]*$"); // alphaNumeric + ( )
    }
    
    /**
     * Check if ip is valid
     * @param ip
     * @return boolean isValidIP
     */
    public static boolean isValidIP(String ip) 
    {
        return ip != null && ip.length() < 45 && isValidIPV4(ip);
    }
    
    /**
     * Check if is valid text length
     * @param text
     * @return boolean isValidText
     */
        public static boolean isValidTextLength(String text) 
    {
        return text != null && text.length() < 10000;
    }
    
}
