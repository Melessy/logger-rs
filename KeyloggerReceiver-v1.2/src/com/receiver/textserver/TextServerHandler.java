/*
 * Copyright (c) 2020, Melessy <https://bitbucket.org/Melessy/> 
 * Email: <melessy@tutamail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.receiver.textserver;

import com.google.gson.Gson;
import com.receiver.Client;
import com.receiver.Constants;
import static com.receiver.Constants.executor;
import static com.receiver.Constants.logger;
import static com.receiver.Constants.mainDir;
import static com.receiver.Constants.seperator;
import com.receiver.utils.DateTime;
import static com.receiver.utils.DateTime.getCurrentDateString;
import static com.receiver.utils.DateTime.getCurrentDateTimeString;
import com.receiver.utils.SendEmail;
import com.receiver.utils.StringUtils;
import java.io.EOFException;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.security.GeneralSecurityException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TextServerHandler implements Runnable
{
    /**
     * Variables
     */
    private Client client;
    private final Socket socket;
    
/**
 * TextServerHandler
 * @param socket 
 */
public TextServerHandler(Socket socket) 
{
        this.socket = socket;
    }

    @Override
    public void run() 
    {
          synchronized(this) 
          {
            try (ObjectInputStream ois = new ObjectInputStream(socket.getInputStream())) // Init ObjectInputStream ois
            {   
                client = (Client) ois.readObject(); // Get serializeable user object
                logger.log(Level.INFO, "[username: {0}] [ip: {1}] [textSize: {2}]", new Object[]{client.username, client.ip, client.text.length()}); // Log username & ip
                logger.log(Level.INFO, "Text received from: {0}_{1} -> {2}", new Object[]{client.username, client.ip, client.text}); // Log text received
                ois.close(); // Close ObjectInputStream
                socket.close(); // Close socket
            }
            catch (EOFException exception) 
            {
                Logger.getLogger(TextServerHandler.class.getName()).log(Level.SEVERE, null, exception);
                Logger.getLogger(TextServerHandler.class.getName()).log(Level.WARNING, "Stopping Thread {0}", Thread.currentThread().getId());
                Thread.currentThread().interrupt(); // Interrupt thread
            }
            catch (IOException | ClassNotFoundException ex) 
            {
               Logger.getLogger(TextServerHandler.class.getName()).log(Level.SEVERE, null, ex);
               Logger.getLogger(TextServerHandler.class.getName()).log(Level.WARNING, "Stopping Thread {0}", Thread.currentThread().getId());
               Thread.currentThread().interrupt(); // Interrupt thread
           }
            
               /**
                * Check if valid ->
                * username
                * IP
                * text
                */
               if (StringUtils.isValidUsername(client.username) 
                       && StringUtils.isValidIP(client.ip) 
                       && StringUtils.isValidTextLength(client.text)) 
               {
                    System.out.println("Valid client!"); // Print valid client
           
               /**
                * If sendLogsOverEmail is enabled in constants.
                * send received text with email
                */
               if (Constants.sendLogsOverEmail) 
               {
                    try 
                    {
                        SendEmail.sendText(client.username, client.ip, client.text);
                    } 
                    catch (GeneralSecurityException | IOException ex) 
                    {
                        Logger.getLogger(TextServerHandler.class.getName()).log(Level.SEVERE, null, ex);
                    }
               }

                /**
                 * Create userDir
                 */
               if (Constants.saveOnDisk)
               {
                File userDir = new File(mainDir, client.username+"_"+client.ip); // The user directory where we store logs
                if (!userDir.exists()) 
                {
                userDir.mkdir();
                }
                
                /**
                 * Create logsDir within userDir
                 */
               File logsDir = new File(userDir, getCurrentDateString()); // The log directory where we store logs based on date
               if (!logsDir.exists()) 
               {
                   logsDir.mkdir();
               }
             
               /**
                * Print/Write received text to file,
                * @Note that getCurrentDateTimeString() : Isn't a valid file format in windows
                */
		try (OutputStream os = Files.newOutputStream(Paths.get(logsDir + seperator + getCurrentDateTimeString()+"_log.txt"), StandardOpenOption.CREATE, StandardOpenOption.WRITE,
				StandardOpenOption.APPEND); PrintWriter writer = new PrintWriter(os)) 
                {
		writer.print(client.text);
                writer.flush(); // Flush writer
                writer.close(); // Close writer
		} 
                catch (IOException ex) 
                {
		Logger.getLogger(TextServerHandler.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
               
            /**
             * Log users to ./data/logs/users.json
             */
           try (OutputStream os = Files.newOutputStream(Paths.get("./data/logs/" + seperator + "users.json"), StandardOpenOption.CREATE, StandardOpenOption.WRITE,
				StandardOpenOption.APPEND); PrintWriter writer = new PrintWriter(os)) 
                {
                     Map<String, Object> map = new HashMap<>(); // Initialize hashMap
                     map.put("username", client.username); // Add username to map
                     map.put("ip", client.ip); // Add ip to map
                     map.put("time", DateTime.getCurrentDateTimeString()); // Add currentDateTime to map
                     new Gson().toJson(map, writer); // Convert map to JSON File
                     writer.write("\n"); // Write new line
                     writer.flush(); // Flush
                     writer.close(); // Close
		} 
                 catch (IOException ex) 
                { 
                  Logger.getLogger(TextServerHandler.class.getName()).log(Level.SEVERE, null, ex);
              }
   
               logger.info(executor.toString()); // Log executor status
    }}}}
