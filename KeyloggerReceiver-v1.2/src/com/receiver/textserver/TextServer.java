/*
 * Copyright (c) 2020, Melessy <https://bitbucket.org/Melessy/> 
 * Email: <melessy@tutamail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.receiver.textserver;

import static com.receiver.Constants.executor;
import static com.receiver.Constants.logger;
import static com.receiver.Constants.portTextServer;
import static com.receiver.firewall.BlockedConnections.isBlockedIp;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TextServer implements Runnable
{      
    
       @Override
       public void run()
       {
        try (ServerSocket serverSocket = new ServerSocket(portTextServer)) // Initialize serverSocket
        {
            logger.log(Level.INFO, "Server is listening on port: {0}", portTextServer); // Log Server listening
            while (true) 
            {
                Socket socket = serverSocket.accept(); // Accept socket connection from client
                socket.setSoTimeout(120000); // Set timeout for 2 minutes
                logger.log(Level.INFO, "New Connection from: {0}", socket.getInetAddress()); // Log new file incomming
                if (isBlockedIp(socket.getInetAddress().getHostAddress()))
                { // Check if ip is blocked in ./data/blockedips.json
                socket.close(); // Close socket if blocked
                logger.log(Level.WARNING, "Client {0} was blocked", socket.getInetAddress()); // Log blocked client
                } 
                else 
                {
                executor.submit(new TextServerHandler(socket)); // Execute TextServerHandler
                logger.log(Level.INFO, "Active Threads: {0}", Thread.activeCount()); // Log active threads
    }}}          
        catch (IOException ex) 
{
               Logger.getLogger(TextServer.class.getName()).log(Level.SEVERE, null, ex);
           }}}
