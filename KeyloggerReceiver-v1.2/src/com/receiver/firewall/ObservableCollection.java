/*
 * Copyright (c) 2016, Gilles
 * Copyright (c) 2020, Melessy <https://bitbucket.org/Melessy/> 
 * Email: <melessy@tutamail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.receiver.firewall;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.Spliterator;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Stream;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;

public class ObservableCollection<T> implements Collection<T>, Observable 
{

    private final Collection<T> collection;
    private final Set<InvalidationListener> listeners = new HashSet<>();

    public ObservableCollection(Collection<T> collection) 
    {
        this.collection = collection;
    }

    @Override
    public int size() 
    {
        return collection.size();
    }

    @Override
    public boolean isEmpty() 
    {
        return collection.isEmpty();
    }

    @Override
    public boolean contains(Object o) 
    {
        return collection.contains(o);
    }

    @Override
    public Iterator<T> iterator() 
    {
        return collection.iterator();
    }

    @Override
    public Object[] toArray() 
    {
        return collection.toArray();
    }

    @Override
    public <T1> T1[] toArray(T1[] a) 
    {
        return collection.toArray(a);
    }

    @Override
    public boolean add(T t) 
    {
        if(collection.add(t)) 
        {
            fireChanged();
            return true;
        }
        return false;
    }

    @Override
    public boolean remove(Object o) 
    {
        if(collection.remove(o)) 
        {
            fireChanged();
            return true;
        }
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) 
    {
        return collection.containsAll(c);
    }

    @Override
    public boolean addAll(Collection<? extends T> c) 
    {
        if(collection.addAll(c)) 
        {
            fireChanged();
            return true;
        }
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) 
    {
        if(collection.removeAll(c)) 
        {
            fireChanged();
            return true;
        }
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) 
    {
        if(collection.retainAll(c)) 
        {
            fireChanged();
            return true;
        }
        return false;
    }

    @Override
    public void clear() 
    {
        collection.clear();
    }

    @Override
    public boolean equals(Object o) 
    {
        return collection.equals(o);
    }

    @Override
    public int hashCode() 
    {
        return collection.hashCode();
    }

    @Override
    public void addListener(InvalidationListener listener) 
    {
        listeners.add(listener);
    }

    @Override
    public void removeListener(InvalidationListener listener) 
    {
        listeners.remove(listener);
    }

    @Override
    public boolean removeIf(Predicate<? super T> filter) 
    {
        if(collection.removeIf(filter)) 
        {
            fireChanged();
            return true;
        }
        return false;
    }

    @Override
    public Spliterator<T> spliterator() 
    {
        return collection.spliterator();
    }

    @Override
    public Stream<T> stream() 
    {
        return collection.stream();
    }

    @Override
    public Stream<T> parallelStream() 
    {
        return collection.parallelStream();
    }

    @Override
    public void forEach(Consumer<? super T> action) 
    {
        collection.forEach(action);
    }

    protected void fireChanged() 
    {
        listeners.forEach(l -> l.invalidated(this));
    }
}

