/*
 * Copyright (c) 2016, Gilles
 * Copyright (c) 2020, Melessy <https://bitbucket.org/Melessy/> 
 * Email: <melessy@tutamail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.receiver.firewall;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class BlockedConnections 
{
        
    	private final static ObservableCollection<String> BLOCKED_IPS = loadList("./data/blockedips.json"); // BLOCKED_IPS File
        
        /**
         * blockedIps getter
         * @return BLOCKED_IPS
         */
        public static ObservableCollection<String> getBlockedIps() 
        {
		return BLOCKED_IPS;
	}
        
        /**
         * isBlockedIp boolean
         * @param ip
         * @return blocked IP boolean
         */
	public static boolean isBlockedIp(String ip) 
        {
		return getBlockedIps().contains(ip);
	}
    
        /**
         * loadList
         * @param fileName
         * @return ObservableCollection
         */
    private static ObservableCollection<String> loadList(String fileName) 
    {
		File file = new File(fileName);
		if (!file.exists())
                {
			saveList(fileName);
			return new ObservableCollection<>(new ArrayList<>());
		}

		try (FileReader fileReader = new FileReader(file)) 
                {
			JsonParser parser = new JsonParser();
			JsonArray object = (JsonArray) parser.parse(fileReader);
			return new ObservableCollection<>(new Gson().fromJson(object, new TypeToken<ArrayList<String>>() {}.getType()));
		} 
                catch (Exception e) 
                {
			Logger.getLogger(BlockedConnections.class.getName()).log(Level.SEVERE, null, e);
			return new ObservableCollection<>(new ArrayList<>());
		}
	}
    
    /**
     * saveList
     * @param fileName 
     */
    private static void saveList(String fileName) 
    {
		saveList(new ObservableCollection<>(new ArrayList<>()), fileName);
	}

	private static void saveList(ObservableCollection<String> list, String fileName) 
        {
		File fileToWrite = new File(fileName);

		if (!fileToWrite.getParentFile().exists()) 
                {
			try 
                        {
				if(!fileToWrite.getParentFile().mkdirs())
					return;
			} 
                        catch (SecurityException e) 
                        {
				System.out.println("Unable to create directory for list file!");
			}
		}

		try (FileWriter writer = new FileWriter(fileToWrite)) 
                {
			Gson builder = new GsonBuilder().setPrettyPrinting().create();
			writer.write(builder.toJson(list, new TypeToken<ObservableCollection<String>>() {}.getType()));
			writer.close();
		} 
                catch (Exception e) 
                {
                        Logger.getLogger(BlockedConnections.class.getName()).log(Level.SEVERE, null, e);
		}
	}
     
   /*public static void addBlockedIp(String ip) throws IOException 
    {
        JSONArray blockedIps = new JSONArray();
        blockedIps.add(ip);
        Files.write(Paths.get("./src/data/blockedips.json"), blockedIps.toJSONString().getBytes());
        //PrintWriter out = new PrintWriter(new FileWriter("./data/blockedips.json"));
    }*/
    
     public static void main(String[] args) throws IOException
    {   
        
        Files.lines(new File("./data/logs/users.json").toPath())
       .map(s -> s.trim())
       .filter(s -> !s.isEmpty())
       .forEach(System.out::println);
        
        getBlockedIps().forEach((ip) -> 
        {
        System.out.println(ip);
        });
}
}
