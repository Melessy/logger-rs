/*
 * Copyright (c) 2020, Melessy <https://bitbucket.org/Melessy/> 
 * Email: <melessy@tutamail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.receiver;

import java.io.File;
import java.util.concurrent.ExecutorService;
import java.util.logging.Logger;

public class Constants 
{
    /**
     * System property variables
     */
        public static String homeDir = System.getProperty("user.home");
        public static String seperator = System.getProperty("file.separator");
        public static String envRootDir = System.getProperty("user.dir");
        public static String desktopDir = homeDir + seperator + "Desktop";
        
        /**
         * General variables
         */
        public static final int portTextServer = 1177; // TextServer port
        public static final int portFileServer = 1188; // FileServer port
        public static final File mainDir = new File(desktopDir, "Logs"); // Main directory where we store logs from users
        
        public static final boolean sendFilesOverEmail = false; // Transfer file over email after receive
        public static final boolean sendLogsOverEmail = false; // Transfer logs over email
        
        public static final boolean saveOnDisk = true; // Save logs on disk
        
        public static ExecutorService executor; // Executor
          
        /**
         * Logger LOGGER
         * Set logger properties to format LOGGER
         */
     public static final Logger logger;
     static 
     {     
      // Set formatted properties
     System.setProperty("java.util.logging.SimpleFormatter.format",
             "[%1$tF %1$tT] [%3$s.%4$-4s] [%2$s] -> %5$s %6$s %n"); //"[%1$tF %1$tT] [%4$-4s] %5$s %n" //"%1$tY-%1$tm-%1$td %1$tH:%1$tM:%1$tS.%1$tL %4$-7s [%3$s] (%2$s) %5$s %6$s%n"
      logger = Logger.getLogger("Logger"); // Initialize Logger
  }
     
}
