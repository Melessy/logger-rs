/*
 * Copyright (c) 2020, Melessy <https://bitbucket.org/Melessy/> 
 * Email: <melessy@tutamail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.receiver;

import java.io.Serializable;

/**
 * Serializable interface
 */
public final class Client implements Serializable 
{
    /**
     * Variables
     */
        public static final long serialVersionUID = 1L;
    
	public String username;
        public String ip;
        public String text;
        
        public byte[] fileContent;
        public String fileName;
        
        /**
         * Serializable for TextServer
         * @see TextServerHandler.java
         * @param username
         * @param ip
         * @param text 
         */
	Client(String username, String ip, String text)
        {
		this.username = username;
                this.ip = ip;
                this.text = text;
}
        
        /**
         * Serializable for FileServer
         * @see FileServerHandler.java
         * @param username
         * @param ip
         * @param fileContent, Content of the file in bytes
         * @param fileName
         */
        Client(String username, String ip, byte[] fileContent, String fileName)
        {
		this.username = username;
                this.ip = ip;
                this.fileContent = fileContent;
                this.fileName = fileName;
}
}
