/*
 * Copyright (c) 2020, Melessy <https://bitbucket.org/Melessy/> 
 * Email: <melessy@tutamail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.keylogger.utils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;

/**
 * @TODO Rewrite/Remove
 * @NOTE Currently Unused
 * @deprecated Remove
 */
public class EncryptionUtils 
{
        private static final char[] PASSWORD = "/!IAMT#h/0e!2k20i#N9melessy7/#1".toCharArray();
        private static final byte[] SALT = { (byte) 0xde, (byte) 0x33, (byte) 0x10, (byte) 0x12, (byte) 0xde, (byte) 0x33,
            (byte) 0x10, (byte) 0x12, };
        private static final String instance = "PBEWithMD5AndTripleDES";
        
     public static void main(String[] args)
    {
    }
     
    public static String encrypt(String property) 
    {
            try 
            {
                SecretKeyFactory keyFactory = SecretKeyFactory.getInstance(instance);
                SecretKey key = keyFactory.generateSecret(new PBEKeySpec(PASSWORD));
                Cipher pbeCipher = Cipher.getInstance(instance);
                pbeCipher.init(Cipher.ENCRYPT_MODE, key, new PBEParameterSpec(SALT, 20));
                return base64Encode(pbeCipher.doFinal(property.getBytes("UTF-8")));
            }
            catch (NoSuchAlgorithmException | InvalidKeySpecException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | UnsupportedEncodingException | IllegalBlockSizeException | BadPaddingException ex) 
            {
                Logger.getLogger(EncryptionUtils.class.getName()).log(Level.SEVERE, null, ex);
            }
            return null;
    }
     
      private final static char[] ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".toCharArray();
        private static final int[]  toInt   = new int[128];
        static 
        {
            for(int i=0; i< ALPHABET.length; i++) 
            {
                toInt[ALPHABET[i]]= i;
            }
        }
    private static String base64Encode(byte[] bytes) 
    {
        int size = bytes.length;
            char[] ar = new char[((size + 2) / 3) * 4];
            int a = 0;
            int i=0;
            while(i < size){
                byte b0 = bytes[i++];
                byte b1 = (i < size) ? bytes[i++] : 0;
                byte b2 = (i < size) ? bytes[i++] : 0;

                int mask = 0x3F;
                ar[a++] = ALPHABET[(b0 >> 2) & mask];
                ar[a++] = ALPHABET[((b0 << 4) | ((b1 & 0xFF) >> 4)) & mask];
                ar[a++] = ALPHABET[((b1 << 2) | ((b2 & 0xFF) >> 6)) & mask];
                ar[a++] = ALPHABET[b2 & mask];
            }
            switch(size % 3) 
            {
                case 1: ar[--a]  = '=';
                case 2: ar[--a]  = '=';
            }
            return new String(ar);
    }

    public static String decrypt(String property)
    {
            try 
            {
                SecretKeyFactory keyFactory = SecretKeyFactory.getInstance(instance);
                SecretKey key = keyFactory.generateSecret(new PBEKeySpec(PASSWORD));
                Cipher pbeCipher = Cipher.getInstance(instance);
                pbeCipher.init(Cipher.DECRYPT_MODE, key, new PBEParameterSpec(SALT, 20));
                return new String(pbeCipher.doFinal(base64Decode(property)), "UTF-8");
            } 
            catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException | IOException | InvalidKeySpecException ex) 
            {
                Logger.getLogger(EncryptionUtils.class.getName()).log(Level.SEVERE, null, ex);
            }
            return null;
    }

    private static byte[] base64Decode(String s) throws IOException 
    {
          int delta = s.endsWith( "==" ) ? 2 : s.endsWith( "=" ) ? 1 : 0;
            byte[] buffer = new byte[s.length()*3/4 - delta];
            int mask = 0xFF;
            int index = 0;
            for(int i=0; i< s.length(); i+=4) 
            {
                int c0 = toInt[s.charAt( i )];
                int c1 = toInt[s.charAt( i + 1)];
                buffer[index++]= (byte)(((c0 << 2) | (c1 >> 4)) & mask);
                if(index >= buffer.length) 
                {
                    return buffer;
                }
                int c2 = toInt[s.charAt( i + 2)];
                buffer[index++]= (byte)(((c1 << 4) | (c2 >> 2)) & mask);
                if(index >= buffer.length) 
                {
                    return buffer;
                }
                int c3 = toInt[s.charAt( i + 3 )];
                buffer[index++]= (byte)(((c2 << 6) | c3) & mask);
            }
            return buffer;
        }
    }
